class TriggerElement {

    constructor( element, attribute ) {
        this.element = element;
        this.attribute = attribute;
        this.selector = element.getAttribute(this.attribute);

        this.updateAriaExpanded = this.updateAriaExpanded.bind(this);

        // Force ARIA by default.
        this.updateAriaExpanded(false);
    }

    updateAriaExpanded(isExpanded){
        this.element.setAttribute('aria-expanded', isExpanded)
    }

    getTargetElement(){
        return document.querySelector(this.selector);
    }
}

export default TriggerElement;
