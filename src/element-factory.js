import NavigationElement from './navigation-element'
import SubmenuElement from './submenu-element'
import TriggerElement from './trigger-element'
import MenuElement from './menu-element'

function createNavigationElements(triggers, isOpenClassname) {
    let navigations = [];

    triggers.forEach((trigger) => {
        let el = trigger.getTargetElement();
        let navEl = navigations.find((targetEl) => (targetEl.element === el));
        let isTargetNav = el.tagName === 'NAV';

        if (isTargetNav && !navEl) {

            navEl = new NavigationElement(el, isOpenClassname);
        }

        if (isTargetNav) {
            navEl.triggers.push(trigger);
            navigations.push(navEl);
        }
    });

    return navigations;
}

function createSubmenuElements(triggers, isOpenClassname, menu) {

    triggers.forEach((trigger) => {
        let el = trigger.getTargetElement();
        let submenuEl = menu.submenus.find((targetEl) => (targetEl.element === el));
        let isTargetUl = el.tagName === 'UL';

        if (isTargetUl && !submenuEl) {
            submenuEl = new SubmenuElement(el, isOpenClassname);
        }

        if (isTargetUl && menu.element.contains(submenuEl.element)) {
            submenuEl.triggers.push(trigger);
            menu.submenus.push(submenuEl);
        }
    });

    return menu;
}

function createTriggerElements(container, triggerAttribute) {
    let triggers = []
    let triggerElements = Array.from(container.querySelectorAll(`[${triggerAttribute}]`));
    triggerElements.forEach((triggerElement) => {
        let trigger = new TriggerElement(triggerElement, triggerAttribute)
        triggers.push(trigger);
    });

    return triggers;
}

function createMenuElements(navigation) {
    let menuElements = navigation.element.parentNode.querySelectorAll(`#${navigation.element.getAttribute('id')} > ul`);
    return Array.from(menuElements).map((menuEl) => {
        return new MenuElement(menuEl)
    });
}

export {
    createTriggerElements,
    createNavigationElements,
    createMenuElements,
    createSubmenuElements
}
