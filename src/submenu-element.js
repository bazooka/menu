import {focusableStack, FocusableElement} from '@fantassin/focusable'

class SubmenuElement extends FocusableElement {

    constructor(element, isOpenClassname) {

        if (!element) {
            console.error('No element found')
        }

        if (!isOpenClassname) {
            console.warn('No open classname defined.')
        }

        super(element)

        this.element = element;
        this.triggers = [];
        this.isOpenClassname = isOpenClassname;
        this.isOpen = false;

        this.toggleOpen = this.toggleOpen.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.updateAriaHidden = this.updateAriaHidden.bind(this);
        this.init = this.init.bind(this);
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    init() {
        super.init()
        this.updateAriaHidden(true) // In case is not mentionned in DOM

        this.triggers.forEach((trigger) => {
            trigger.element.addEventListener('click', this.toggleOpen)
        })
    }

    toggleOpen(e) {
        e.preventDefault();

        if (this.isOpen) {
            this.close()
            return;
        }

        this.open();
    }

    isMobile() {
        return window.matchMedia('(max-width: 1024px)').matches
    }

    updateAriaHidden(isHidden) {
        this.isOpen = !isHidden
        if (this.isMobile()) {
            this.element.setAttribute('aria-hidden', isHidden) // Update a11y
        }
    }

    handleKeyDown(e) {
        if (e.key === 'Escape') {
            this.close()
        } else {
            super.trapFocus(e)
        }
    }

    open() {
        window.addEventListener('keydown', this.handleKeyDown)
        // ARIA
        this.updateAriaHidden(false)
        this.triggers.forEach((trigger) => {
            trigger.updateAriaExpanded(true);
        })
        focusableStack.setCurrent(this)
        this.isOpen = true;
        this.element.classList.add(this.isOpenClassname);
        // Create and dispatch the event.
        let event = new CustomEvent("submenuOpened", {
            detail: this
        });
        document.dispatchEvent(event);
    }

    close() {
        this.triggers.forEach((trigger) => {
            trigger.updateAriaExpanded(false);
        })
        this.updateAriaHidden(true)
        this.isOpen = false;
        this.element.classList.remove(this.isOpenClassname);
        window.removeEventListener('keydown', this.handleKeyDown)
        focusableStack.restorePrevious()
        let event = new CustomEvent("submenuClosed", {
            detail: this
        });
        document.dispatchEvent(event);
    }
}

export default SubmenuElement;
