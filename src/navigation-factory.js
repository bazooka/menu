import MenuElement from './menu-element'
import TriggerElement from "./trigger-element";
import {createNavigationElements, createTriggerElements, createMenuElements, createSubmenuElements} from "./element-factory";

class NavigationFactory {

    constructor({triggerAttribute, isOpenClassname, htmlScrollLockClassname}) {
        this.triggers = [];
        this.navigations = [];
        this.triggerAttribute = triggerAttribute;
        this.htmlScrollLockClassname = htmlScrollLockClassname;
        this.isOpenClassname = isOpenClassname;

        this.init = this.init.bind(this);
        this.createTriggers = this.createTriggers.bind(this);
        this.createNavigationElements = this.createNavigationElements.bind(this);
        this.createMenuElements = this.createMenuElements.bind(this);
        this.createSubmenuElements = this.createSubmenuElements.bind(this);
        this.closeAllMenuWithoutCurrent = this.closeAllMenuWithoutCurrent.bind(this);
        this.handleNavigationOpened = this.handleNavigationOpened.bind(this);
        this.handleNavigationClosed = this.handleNavigationClosed.bind(this);
        this.hasSubmenusOpen = this.hasSubmenusOpen.bind(this);
    }

    init() {
        this.createTriggers();
        this.createNavigationElements();
        this.createMenuElements();
        this.createSubmenuElements();

        this.navigations.forEach((navigation) => {
            navigation.init();
        })

        document.addEventListener('navigationOpened', this.handleNavigationOpened);
        document.addEventListener('navigationClosed', this.handleNavigationClosed);
    }

    createTriggers() {
        this.triggers = createTriggerElements(document, this.triggerAttribute)
    }

    createNavigationElements() {
        this.navigations = createNavigationElements(this.triggers, this.isOpenClassname);
    }

    createMenuElements() {
        this.navigations.map( (navigation) => {
            navigation.menus = createMenuElements(navigation);
            return navigation;
        });
    }

    createSubmenuElements() {
        this.navigations.map( (navigation) => {
            navigation.menus.map( (menu) => {
                return createSubmenuElements(this.triggers, this.isOpenClassname, menu);
            } )
            return navigation;
        });

    }

    handleNavigationClosed(e) {
        if ( !e.detail.isOpen ) {
            document.documentElement.classList.remove(this.htmlScrollLockClassname);
            return;
        }

        document.documentElement.classList.add(this.htmlScrollLockClassname);
    }

    handleNavigationOpened(e) {
        this.closeAllMenuWithoutCurrent(e.detail);
        this.handleNavigationClosed(e);
    }

    closeAllMenuWithoutCurrent(currentNavigation) {
        this.navigations.forEach((navigation) => {
            let keepOpenClassname = this.hasSubmenusOpen(currentNavigation) && !currentNavigation.isOpen || currentNavigation.isOpen
            let hasToBeRemoved = !keepOpenClassname;

            if (hasToBeRemoved) {
                navigation.close();
            }
        })
    }

    hasSubmenusOpen(navigation) {
        let hasAtLeastOneSubmenuOpened = false;
        navigation.menus.forEach((menu) => {
            menu.submenus.forEach((submenu) => {
                if (submenu.isOpen) {
                    hasAtLeastOneSubmenuOpened = true;
                }
            })
        })

        return hasAtLeastOneSubmenuOpened;
    }
}

export default NavigationFactory;
