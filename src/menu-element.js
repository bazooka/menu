import {focusableStack, FocusableElement} from '@fantassin/focusable'

class MenuElement {

    constructor(ulElement) {

        this.element = ulElement;
        this.submenus = [];

        this.init = this.init.bind(this);
    }

    init() {

        if( this.hasSubmenus() ){
            this.submenus.forEach((submenu) => {
                submenu.init();
            })
        }
    }

    hasSubmenus() {
        return this.submenus.length > 0;
    }
}

export default MenuElement;
