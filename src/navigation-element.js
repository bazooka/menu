import {focusableStack, FocusableElement} from '@fantassin/focusable'

class NavigationElement extends FocusableElement {

    constructor(element, isOpenClassname) {

        if (!element) {
            console.warn('No element found.')
        }

        if (!isOpenClassname) {
            console.warn('No open classname defined.')
        }

        super(element)

        this.element = element;
        this.isOpenClassname = isOpenClassname;
        this.isOpen = false;
        this.triggers = [];
        this.menus = [];

        this.toggleOpen = this.toggleOpen.bind(this);
        this.handleClickOutsideContainer = this.handleClickOutsideContainer.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.init = this.init.bind(this);
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    init() {
        super.init()
        // In case is not mentionned in DOM.s
        this.updateAriaHidden(true)

        this.triggers.forEach( (trigger) => {
            trigger.element.addEventListener('click', this.toggleOpen)
        } )

        if( this.hasMenus() ){
            this.menus.forEach((menu) => {
                menu.init();
            })
        }
    }

    hasMenus() {
        return this.menus.length > 0;
    }

    toggleOpen(e) {
        e.preventDefault();

        if (this.isOpen) {
            this.close()
            return;
        }

        this.open();
    }

    handleClickOutsideContainer(e) {
        let targetElement = e.target;

        do {
            let targetElementIsATrigger = this.triggers.find( trigger => trigger.element === targetElement );
            if (targetElement == this.element || !! targetElementIsATrigger) {
                // This is a click inside. Do nothing, just return.
                return
            }
            // Go up the DOM
            targetElement = targetElement.parentNode
        } while (targetElement)

        if (this.isOpen && ! this.element.isSubmenu) {
            // This is a click outside.
            this.close()
        }
    }

    isMobile() {
        return window.matchMedia('(max-width: 1024px)').matches
    }

    updateAriaHidden(isHidden) {
        this.isOpen = !isHidden
        if (this.isMobile()) {
            this.element.setAttribute('aria-hidden', isHidden) // Update a11y
        }
    }

    handleKeyDown(e) {
        if (e.key === 'Escape') {
            this.close()
        } else {
            super.trapFocus(e)
        }
    }

    open() {
        document.addEventListener('click', this.handleClickOutsideContainer)
        window.addEventListener('keydown', this.handleKeyDown)
        // ARIA
        this.updateAriaHidden(false)
        this.triggers.forEach( (trigger) => {
            trigger.updateAriaExpanded(true);
        });
        focusableStack.setCurrent(this)
        this.isOpen = true;
        this.element.classList.add(this.isOpenClassname);
        // Create and dispatch the event.
        let event = new CustomEvent("navigationOpened", {
            detail: this
        });
        document.dispatchEvent(event);
    }

    close() {
        this.triggers.forEach( (trigger) => {
            trigger.updateAriaExpanded(false);
        })
        this.updateAriaHidden(true)
        this.isOpen = false;
        this.element.classList.remove(this.isOpenClassname);
        document.removeEventListener('click', this.handleClickOutsideContainer)
        window.removeEventListener('keydown', this.handleKeyDown)
        focusableStack.restorePrevious()
        let event = new CustomEvent("navigationClosed", {
            detail: this
        });
        document.dispatchEvent(event);
    }
}

export default NavigationElement;
