If you want to manage navigation element in your DOM with Vanilla JS.

## 📦 How to install this project ?
```bash
npm install @fantassin/menu
```

## ⚡ How it works ?

```js
import NavigationFactory from '@fantassin/menu'

let navEls = new NavigationFactory({
    triggerAttribute: 'data-trigger-menu',
    isOpenClassname: 'is-open',
    htmlScrollLockClassname: 'is-lock',
})
navEls.init()

```
